%.hex:%.asm
	gpasm $< -o $@

all: 16f628.hex

16f628.hex: 16f628.asm

prog: 16f628.hex
	pk2cmd -Ppic16f628 -F $< -m

run:
	pk2cmd -Ppic16f628 -A5 -r -t
	sleep 2
	pk2cmd -Ppic16f628 -A5
