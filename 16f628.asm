;;; Basic led blinking
;;;
;;; CODE
        errorlevel -302 ; Ignore: Message [302] Register in operand not in bank 0. Ensure bank bits are correct.
processor 16f628
#include <p16f628.inc>

        __CONFIG _WDT_OFF & _INTRC_OSC_NOCLKOUT & _LVP_OFF ; _LVP_OFF needed for portb4

;; Table 4-1 0x20 - 0x7f in bank 0
        cblock 0x20
        DELAYCOUNT1
        DELAYCOUNT2
        endc

;; RESET VECTOR
        org 0x00
        goto main

;;; MAIN
main	bsf     STATUS,RP0      ; Select bank 1

        movlw   0x00            ;
        movwf   TRISB           ; Set all port B pins to output

        movlw   0x81            ; 10000001
        movwf   OPTION_REG      ; pullup disabled, prescaler 4

	bcf     STATUS,RP0      ; Select bank 0

        bsf     PORTB, 4

loop
        movlw   0x30            ; PINB4&5 0011 0000
        xorwf   PORTB,F
        call    delay512ms
        goto    loop

;;; Helpers (stack is 8 levels)
;;; The stack number indicates the number of stack levels needed to used the function.
        ;; Stack 3
delay1024ms     call delay128ms
                call delay128ms
                call delay128ms
                call delay128ms
delay512ms      call delay128ms
                call delay128ms
                call delay128ms
                call delay128ms
                return

        ;; Stack 2
delay128ms      call delay16ms
                call delay16ms
                call delay16ms
                call delay16ms
delay64ms       call delay16ms
                call delay16ms
delay32ms       call delay16ms
                call delay16ms
                return
        ;; Stack 1
delay16ms       call delay2ms
                call delay2ms
                call delay2ms
                call delay2ms
delay8ms        call delay2ms
                call delay2ms
delay4ms        call delay2ms
                call delay2ms
                return
        ;; Stack 0
DELAY_MACRO     MACRO
                decfsz  DELAYCOUNT1,1
                goto    $-1
                decfsz  DELAYCOUNT2,1
                goto    $-3
                ENDM

delay2ms        movlw   0x4D
                movwf   DELAYCOUNT1
                movlw   0x2
                movwf   DELAYCOUNT2
                DELAY_MACRO
delay1ms        movlw   0x4D
                movwf   DELAYCOUNT1
                movlw   0x2
                movwf   DELAYCOUNT2
                DELAY_MACRO
                return
delay500us      movlw   d'77'    ; 2x250us says velleman
                movwf   DELAYCOUNT1
                movlw   d'1'
                movwf   DELAYCOUNT2
                DELAY_MACRO
                return
        end
